#include "mem.h"
#include "mem_internals.h"

#include <stdbool.h>

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

enum test_res{
    HEAP_INIT_FAILED,
    HEAP_INIT_NULL,
    HEAP_INIT_SIZE_ERR,
    HEAP_INIT_PASS,

    BLOCK_SIGNLE_NEXT_NOT_NULL,
    BLOCK_SMALL,
    BLOCK_IS_FREE,
    BLOCK_LARGE,
    BLOCK_SINGLE_PASS,

    BLOCK_MULTIPLE_LIST_ERROR,
    BLOCK_NEW_REGION_LIST_ERROR,
    BLOCK_MULTIPLE_PASS,

    FREE_NOT_SET,
    FREE_NOT_OCCUPIED,
    FREE_NOT_MERGED,
    FREE_PASS
};

char* test_res_msg[] = {
    [HEAP_INIT_FAILED] = "HEAP_INIT_FAILED",
    [HEAP_INIT_NULL] = "HEAP_INIT_NULL",
    [HEAP_INIT_SIZE_ERR] = "HEAP_INIT_SIZE_ERR",
    [HEAP_INIT_PASS] = "HEAP_INIT_PASS",

    [BLOCK_SIGNLE_NEXT_NOT_NULL] = "BLOCK_SIGNLE_NEXT_NOT_NULL",
    [BLOCK_SMALL] = "BLOCK_SMALL",
    [BLOCK_IS_FREE] = "BLOCK_IS_FREE",
    [BLOCK_LARGE] = "BLOCK_LARGE",
    [BLOCK_SINGLE_PASS] = "BLOCK_SINGLE_PASS",

    [BLOCK_MULTIPLE_LIST_ERROR] = "BLOCK_MULTIPLE_LIST_ERROR",
    [BLOCK_NEW_REGION_LIST_ERROR] = "BLOCK_NEW_REGION_LIST_ERROR",
    [BLOCK_MULTIPLE_PASS] = "BLOCK_MULTIPLE_PASS",

    [FREE_NOT_SET] = "FREE_NOT_SET",
    [FREE_NOT_OCCUPIED] = "FREE_NOT_OCCUPIED",
    [FREE_NOT_MERGED] = "FREE_NOT_MERGED",
    [FREE_PASS] = "FREE_PASS"
};

void print_test_res_msg(enum test_res res){
    fprintf(stderr, "%s \n", test_res_msg[res]);
}

void print_test_res(char* msg, enum test_res (*test_func)(void)){
    printf("%s: \n", msg);
    print_test_res_msg(test_func());
    printf("\n");
}



enum test_res test_heap_init(void){
    size_t size = 2*REGION_MIN_SIZE;
    struct block_header* block = heap_init(size);

    if (block == NULL){
        return HEAP_INIT_NULL;
    }
    
    if(size_from_capacity(block->capacity).bytes < size){
        return HEAP_INIT_SIZE_ERR;
    }

    heap_term();
    return HEAP_INIT_PASS;
}

enum test_res test_malloc_single(void){
    struct block_header* initial = heap_init(REGION_MIN_SIZE);
    size_t full_size = initial->capacity.bytes;     //block with max size for this region

    void* content = _malloc(full_size);
    struct block_header* block = block_get_header(content);

    if (block->next != NULL){
        debug_heap(stderr, initial);
        return BLOCK_SIGNLE_NEXT_NOT_NULL;
    }

    if(block->capacity.bytes < full_size){
        debug_heap(stderr, initial);
        return BLOCK_SMALL;
    }

    if(block->capacity.bytes > full_size){
        debug_heap(stderr, initial);
        return BLOCK_LARGE;
    }

    if(block->is_free){
        debug_heap(stderr, initial);
        return BLOCK_IS_FREE;
    }

    heap_term();
    return BLOCK_SINGLE_PASS;
}

enum test_res test_malloc_multiple(void){
    struct block_header* initial = heap_init(REGION_MIN_SIZE);
    size_t size = initial->capacity.bytes / 3;

    struct block_header* b1 = block_get_header(_malloc(size));
    struct block_header* b2 = block_get_header(_malloc(size));
    struct block_header* b3 = block_get_header(_malloc(size));

    struct block_header* b4 = block_get_header(_malloc(size));

    if (b1->next != b2 || b2->next != b3){
        debug_heap(stderr, initial);
        return BLOCK_MULTIPLE_LIST_ERROR;
    }

    if (b3->next != b4){
        debug_heap(stderr, initial);
        return BLOCK_NEW_REGION_LIST_ERROR;
    }

    heap_term();
    return BLOCK_MULTIPLE_PASS;
}

enum test_res test_free(void){
    struct block_header* initial = heap_init(REGION_MIN_SIZE);
    size_t inital_capacity = initial->capacity.bytes;
    size_t size = inital_capacity / 4;

    void* a1 = _malloc(size);
    void* a2 = _malloc(size);
    void* a3 = _malloc(size);
    struct block_header* b1 =   block_get_header(a1);
                                block_get_header(a2);
    struct block_header* b3 =   block_get_header(a3);

    _free(a3);
    if(!(b3->is_free)){
        debug_heap(stderr, initial);
        return FREE_NOT_SET;
    }

    void* a3_new = _malloc(size);
    struct block_header* b3_new = block_get_header(a3_new);
    if(b3_new != b3){
        debug_heap(stderr, initial);
        return FREE_NOT_OCCUPIED;
    }

    _free(a2);
    _free(a3_new);
    _free(a1);
    if(b1->capacity.bytes != inital_capacity){
        debug_heap(stderr, initial);
        return FREE_NOT_MERGED;
    }

    _free(NULL);

    heap_term();
    return FREE_PASS;
}





int main()
{
    print_test_res("Heap init test", &test_heap_init);
    print_test_res("Single block test", &test_malloc_single);
    print_test_res("Multiple block test", &test_malloc_multiple);
    print_test_res("Free test", &test_free);
    return 0;
}
