#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );
/*Размер всего заголовка + смещение до основного содержания*/
const size_t header_size = offsetof( struct block_header, contents );


static bool block_is_big_enough( size_t query, struct block_header* block ){ 
  return block->capacity.bytes >= query; 
}

static size_t pages_count( size_t mem ){
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t round_pages( size_t mem ){
   return getpagesize() * pages_count( mem );
}

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {    
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { 
  return size_max( round_pages( query ), REGION_MIN_SIZE ); 
}

extern inline bool region_is_invalid( const struct region* r );





static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t region_size = region_actual_size(query + header_size);

  bool extends = true;      //extends can be true for the very first region

  void* region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
  if (region_addr == MAP_FAILED){
    extends = false;
    region_addr = map_pages(addr, region_size, 0);
    if (region_addr == MAP_FAILED) return REGION_INVALID;
  }

  block_init(region_addr, (block_size){.bytes = region_size}, NULL);

  struct region r = {.addr = region_addr, .size = region_size, .extends = extends};
  return r;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

static bool try_merge_with_next( struct block_header* block );
/*  освободить всю память, выделенную под кучу */
void heap_term( void ) {
  struct block_header* start_block = (struct block_header*) HEAP_START;

  struct block_header* block = start_block; 
  while(block){
    block->is_free = true;
    block = block->next;
  }

  block = start_block;
  while(block){
    while(try_merge_with_next(block));
    block = block->next;
  }

  block = start_block;
  while(block){
    struct block_header* next = block->next;
    munmap((void*)block, size_from_capacity(block->capacity).bytes);    //note: do not check res of munmap
    block = next;
  }
  
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block && block->is_free && header_size + query + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block_splittable(block, query)){
    size_t size_f = header_size + query;
    size_t size_s = size_from_capacity(block->capacity).bytes - size_f;
    void* second = (void*)block + size_f;

    block_init(second, (block_size){.bytes = size_s}, block->next);
    block_init(block, (block_size){.bytes = size_f}, second);
    return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst && snd && fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!block) return false;

  struct block_header* f = block;
  struct block_header* s = block->next;
  if(mergeable(f, s) && s != NULL){         //add another check special for linter
    size_t size = size_from_capacity(f->capacity).bytes + size_from_capacity(s->capacity).bytes;
    block_init(f, (block_size){.bytes = size}, s->next);
    return true;
  }
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {
    BSR_FOUND_GOOD_BLOCK, 
    BSR_REACHED_END_NOT_FOUND, 
    BSR_CORRUPTED
   } type;
  struct block_header* block;
};

/*Начиная с данного блока, ищет первый подходящий, иначе достигает конца и возвращает последний блок*/
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
  if(!block) return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};
  while(true){
    while(try_merge_with_next(block));
    if(block_is_big_enough(sz, block) && block->is_free){
      return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = block};
    }
    if (block->next == NULL) break;   //check here to save block pointer as last (not null)
    block = block->next;
  }
  return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = block};
}

/*  Попробовать выделить память в куче, начиная с блока `block`, не пытаясь расширить кучу.
 Можно переиспользовать, как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result res = find_good_or_last(block, query);
  if (res.type == BSR_FOUND_GOOD_BLOCK){
    split_if_too_big(res.block, query);
    res.block->is_free = false;
  }
  return res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) return NULL;

  struct region new_region = alloc_region(block_after(last), query);
  if (region_is_invalid(&new_region)) return NULL;
  last->next = new_region.addr;

  if (new_region.extends && try_merge_with_next(last)) return last;

  return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (!heap_start) return NULL;
  query = size_max(query, BLOCK_MIN_CAPACITY);
  
  struct block_search_result in_existing = try_memalloc_existing(query, heap_start);
  switch (in_existing.type)
  {
    case BSR_FOUND_GOOD_BLOCK:
      return in_existing.block;

    case BSR_REACHED_END_NOT_FOUND:{
      struct block_header* new_block = grow_heap(in_existing.block, query);
      struct block_search_result in_new = try_memalloc_existing(query, new_block);
      if(in_new.type == BSR_FOUND_GOOD_BLOCK){
        return in_new.block;
      }
      return NULL;
    }

      
    default:
      return NULL;
  }

  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-header_size);
}

void _free( void* mem ) {
  if (!mem) return;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
